<?php

/**
 * @file providing the service export content xml.
 *
 */

namespace  Drupal\export_xml_writer;
use Drupal\node\Entity\Node;

class ExportXml {

    protected $content;

    public function __construct() {
        $this->content = 'article';
    }

    public function  exportContent($content_type = ''){
        if (empty($content_type)) {
            $type =  $this->content;
        }
        else {
           $type = $content_type;
        }
        //return "Hello " . $type . "!";

        $query = \Drupal::entityQuery('node')
            ->condition('type', $type)
            ->sort('created', 'DESC')
            ->condition('status', 1);

        $filter_nids = $query->execute();
        $nodes = Node::loadMultiple($filter_nids);

        $writer = new \XMLWriter();
        // file by date date('Y-m-d_H-i')
        $file_name = 'sites/default/files/xml/'.$type.'.xml';
        $writer->openURI($file_name);
        $writer->startDocument('1.0', 'UTF-8');

        //Tag Articles
        $writer->startElement("nodes");
        
        foreach ($nodes as $node) {
            //Tag Article
            $writer->startElement('node');

            //tag ID
            $writer->startElement('id');
            $writer->Text($node->nid->value);
            $writer->endElement();

            //Tag Title
            $writer->startElement('title');
            $writer->Text($node->title->value);
            $writer->endElement();

            $writer->endElement();
        }
        $writer->endElement();

        $writer->endDocument();

        $writer->flush();
        //return 'test';
        header('Location:' . $file_name);
    }



}